using System.Configuration;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Libmongocrypt;
using Newtonsoft.Json.Bson;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Data;
using System.Linq;

namespace API.DataAccess
{
    public class MongoDBRepository
    {
        private static MongoClient client;
        private static IMongoDatabase threadDatabase;
        private static string _threadDatabaseId = "Boards";
        private static IMongoCollection<ThreadModel> threadCollection;

        public static void Initialize()
        {
            // looks strange because & is not allowed in app.config
            string ConnectionString = $"{ConfigurationManager.ConnectionStrings["MongoDBConnectionString"].ConnectionString}&w=majority ";
            client = new MongoClient(ConnectionString);
            threadDatabase = client.GetDatabase(_threadDatabaseId);
        }

        public List<string> GetBoards()
        {
            List<string> collections = new List<string>();

            foreach (BsonDocument collection in threadDatabase.ListCollectionsAsync().Result.ToListAsync<BsonDocument>().Result)
            {
                string name = collection["name"].AsString;
                collections.Add(name);
            }

            return collections;
        }

        public ThreadModel GetThread(string collectionId, string id)
        {
            threadCollection = threadDatabase.GetCollection<ThreadModel>(collectionId);
            var filter = Builders<ThreadModel>.Filter.Eq("_id", id);
            return threadCollection.Find(filter).FirstOrDefault();
        }

        public List<ThreadModel> GetAllThreads(string collectionId)
        {
            threadCollection = threadDatabase.GetCollection<ThreadModel>(collectionId);
            var threads = threadCollection.Find(new BsonDocument()).ToList();
            return (threads.Count == 0) ? null : threads;
        }

        public async Task CreateThread(string collectionId, ThreadModel thread)
        {
            threadCollection = threadDatabase.GetCollection<ThreadModel>(collectionId);
            await threadCollection.InsertOneAsync(thread);
        }

        public async Task CreateComment(string collectionId, string id, Comment comment)
        {
            threadCollection = threadDatabase.GetCollection<ThreadModel>(collectionId);

            ThreadModel Updated = GetThread(collectionId, id);
            Comment[] newCommentArr = new Comment[] { comment };
            if(Updated.Comments == null)
                Updated.Comments = newCommentArr;
            else
                Updated.Comments = Updated.Comments.Concat(newCommentArr).ToArray();

            var filter = Builders<ThreadModel>.Filter.Eq("_id", id);
            await threadCollection.ReplaceOneAsync(filter, Updated);
        }
    }
}
