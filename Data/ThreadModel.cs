using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace API.Data
{
    public class ThreadModel
    {
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "ThreadName")]
        [Required]
        [MinLength(1, ErrorMessage = "To Short")]
        public string ThreadName { get; set; }

        [JsonProperty(PropertyName = "ThreadCreator")]
        [Required]
        [MinLength(1, ErrorMessage = "To Short")]
        public string ThreadCreator { get; set; }

        [JsonProperty(PropertyName = "ThreadText")]
        [Required]
        [MinLength(1, ErrorMessage = "To Short")]
        public string ThreadText { get; set; }

        [JsonProperty(PropertyName = "Comments")]
        public Comment[] Comments { get; set; }

        [JsonProperty(PropertyName = "CreationDate")]
        public Int64 CreationDate { get; set; }

        [JsonProperty(PropertyName = "ImageId")]
        public string ImageId { get; set; }

        [JsonProperty(PropertyName = "FileType")]
        public string FileType { get; set; } //image or video

    }
    public class Comment
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "CommentCreator")]
        [Required]
        [MinLength(1, ErrorMessage = "To Short")]
        public string CommentCreator { get; set; }

        [JsonProperty(PropertyName = "CommentText")]
        [Required]
        [MinLength(1, ErrorMessage = "To Short")]
        public string CommentText { get; set; }

        [JsonProperty(PropertyName = "CreationDate")]
        public Int64 CreationDate { get; set; }

        [JsonProperty(PropertyName = "ImageId")]
        public string ImageId { get; set; }

        [JsonProperty(PropertyName = "FileType")]
        public string FileType { get; set; } //image or video
    }
}
