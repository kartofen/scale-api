using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace API.Data
{
    public class imgfb
    {
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }
        [JsonProperty(PropertyName = "base64")]
        public string base64 { get; set; }
    }
    public class imglinkfb
    {
        [JsonProperty(PropertyName = "imglink")]
        public string imglink { get; set; }
    }
}
