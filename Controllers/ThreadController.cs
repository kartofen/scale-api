using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Data;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Linq;
using System;
using Microsoft.Extensions.Hosting;
using API.DataAccess;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Cors;

namespace API.Controllers
{
    [EnableCors("AllowOrigin")]
    [ApiController]
    public class ThreadController: Controller
    {
        MongoDBRepository mongodb = new MongoDBRepository();
        DriveStorageRepository drive = new DriveStorageRepository();

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("api/get/board/{collectionId}")]
        public ActionResult<List<ThreadModel>> GetThreads(string collectionId)
        {
            var t = mongodb.GetAllThreads(collectionId);
            return (t == null) ? NotFound() : t;
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("api/get/thread/{collectionId}/{id}")]
        public ActionResult<ThreadModel> GetThread(string collectionId, string id)
        {
            var t = mongodb.GetThread(collectionId, id);
            return (t == null) ? NotFound() : t;
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("api/get/boards")]
        public ActionResult<List<string>> GetBoardsNames()
        {
            var t = mongodb.GetBoards();
            return (t == null) ? NotFound() : t;
        }

        [EnableCors("AllowOrigin")]
        [HttpPost]
        [Route("api/post/thread/{collectionId}")]
        public async Task CreateThreadAsync(string collectionId, [FromBody] ThreadModel thread)
        {
            await mongodb.CreateThread(collectionId, thread);
        }

        [EnableCors("AllowOrigin")]
        [HttpPost]
        [Route("api/post/comment/{collectionId}/{id}")]
        public async Task CreateCommentAsync(string collectionId, string id, [FromBody] Comment comment)
        {
            await mongodb.CreateComment(collectionId, id, comment);
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        [Route("api/get/image/{imgid}")]
        public IActionResult GetImageLink(string imgid)
        {
            if(imgid == null || imgid == "") return BadRequest();
            //get image link
            string imglink = drive.GetImageIdFromName(imgid);

            if(imglink == null) return NotFound();
            return Ok(imglink);
        }

        [EnableCors("AllowOrigin")]
        [HttpPost]
        [Route("api/post/image")]
        public void CreateImage([FromBody] imgfb image)
        {
            string fileName = image.name;
            string base64 = image.base64;

            if(fileName == null || fileName == "" ||
               base64   == null || base64   == "") return;

            byte[] imgdata = Convert.FromBase64String(base64); // convert the base64 part

            using(var stream = new MemoryStream(imgdata))
            {
                drive.UploadImage(stream, fileName);
            }
        }
    }
}
